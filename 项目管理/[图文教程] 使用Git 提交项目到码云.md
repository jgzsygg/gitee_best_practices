### 1. 环境准备
#### 1.1 本机配置Git Home
##### 1.1.1 本机环境说明
&nbsp; &nbsp; 此处演示使用的Eclipse，系统为Windows 10 以为参考
##### 1.1.2 配置环境变量
&nbsp; &nbsp; 进入控制面板 - 系统与安全 - 系统 - 高级系统设置；系统属性 - 高级 - 环境变量；用户变量 - 新建变量：HOME，值：E:\win10\360downno\UNSoftware\GitHome。我这里路径有些繁琐了；完全可以为E:\GitHome，根据个人习惯自定义。
>*[图1-1 环境变量配置图]*<br/>
![图1-1 环境变量配置图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-1.png)

### 2. 开发工具配置Git和SSH
#### 2.1 通过开发工具配置Git用户信息
(1). 打开Eclipse选中菜单栏Windows下的二级菜单选中选项Preferences
&nbsp; &nbsp; 说明：一般情况下，开发工具的Perferences位置是这样找的；Perferences的位置根据具体的开发平台，功能版本而定。如果开发工具的Perferences下没有Git，需要下载EGit插件到开发平台。
(2). 配置好git的环境变量HOME后，Configuration的Location会默认自动带出GitHome路径；没有的话也可以在Git填充GitHome。单击Add Entry添加用户信息，如下图所示；然后Apply - OK保存设置即可.
>*[图2-1 Git 用户信息配置图]*<br/>
![图2-1 Git 用户信息配置图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-2.png)

#### 2.2. 配置ssh
##### 2.2.1 SSH2 home
&nbsp; &nbsp; SSH2 home使用默认路径也可以，这里使用的是GitHome的路径。都可以的。
>*[图2-2 SSH2 Home 配置图]*<br/>
![图2-2 SSH2 Home 配置图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-3.png)

##### 2.2.2 Key Management - 生成密钥
&nbsp; &nbsp; 按如图标识的顺序生成密钥；其中Passphrase是设置的密码，当使用密钥时会要求输入此密码。点击Save Private Key会生成密钥对，在E:\win10\360downno\UNSoftware\GitHome\.ssh下生成两个文件id_rsa，id_rsa.pub。其中id_rsa保存私钥，id_rsa.pub保存公钥。
>*[图2-3 密钥配置图01]*<br/>
![图2-3 密钥配置图01](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-4.png)

&nbsp; &nbsp; 然后回到general设置单击Add Private Key选中密钥文件id_rsa
>*[图2-4 密钥配置图02]*<br/>
![图2-4 密钥配置图02](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-5.png)

### 3. 配置SSH到码云
#### 3.1 配置SSH公钥到码云
(1). 进入码云，点击设置选中SSH公钥，添加公钥；
>*[图3-1 码云配置公钥图01]*<br/>
![图3-1 码云配置公钥图01](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-6.png)

(2). 标题 - 自定义；公钥 - 记事本打开id_rsa.pub文件，复制所有文本内容。
>*[图3-2 码云配置公钥图02]*<br/>
![图3-2 码云配置公钥图02](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-7.png)

(3). 使用此密钥会提示输入之前设置的密码。到此完成SSH的添加。
>*[图3-3 权限验证图]*<br/>
![图3-3 权限验证图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-8.png)

### 4. 创建一个项目
#### 4.1 新建项目
&nbsp; &nbsp; 选中新建项目，填写项目相关信息。其中路径是唯一的；选择语言 - 此处为Java；添加.gitignore - 此处为Eclipse;添加开源许可证 - 此处为Eclipse；是否公开此处支持公开也支持私有。
>*[图4-1 码云创建项目图]*<br/>
![图4-1 码云创建项目图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-9.png)

#### 4.2 复制项目的SSH地址, Clone 项目到本地仓库
&nbsp; &nbsp; 此处演示项目地址, git@gitee.com:tencentI/DoNotCallHellworld.git
>*[图4-2 码云新建项目地址图]*<br/>
![图4-2 码云新建项目地址图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-10.png)

### 5. Clone项目到本地
#### 5.1 Clone项目到本地
##### 5.1.1 打开视图Git Repositories
&nbsp; &nbsp; 进入开发工具，选择菜单选项Windows - show view - other找到Git Repositories打开。
&nbsp; &nbsp; 注意IDEA 等其他工具与Eclipse 视图菜单路径可能不完全一致.
>*[图5-1 Eclipse 视图Git Repositories 查看图]*<br/>
![图5-1 Eclipse 视图Git Repositories 查看图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-11.png)
*[图5-1 Git Repositories 展开图]*<br/>
![图5-2 Git Repositories 展开图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-12.png)

##### 5.1.2 Clone a Git reponsitory
&nbsp; &nbsp; 这里以Clone为例，也可以新建Create a new ...或者Add an existing...
&nbsp; &nbsp; 将SSH地址填写在URL，就是上面复制的SSH地址，其他信息自动填充；点击Next
>*[图5-3 Clone 项目到一个新的仓库图]*<br/>
![图5-3 Clone 项目到一个新的仓库图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-13.png)

##### 5.1.3 填写使用密钥时设置的密码；然后一路Next然后Finish即可。
>*[图5-4 验证密钥使用者的密码图]*<br/>
![图5-4 验证密钥使用者的密码图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-14.png)

##### 5.1.4 Clone后的项目的基本结构
&nbsp;&nbsp;Clone后的项目的基本结构如下，然后就是基于该项目进行开发了。
>*[图5-5 项目基本结构图]*<br/>
![图5-5 项目基本结构图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-15.png)

### 6. Push项目到码云
#### 6.1 上传本地项目
&nbsp; &nbsp; 这里要说两个基本命令Push和Pull，其中本地进行项目开发后Commit提交更新，然后Push将更新推送到服务器上；每次push的时候我们先pull一下，看有没有其他人更新过代码。起到多人协作开发时的版本控制的作用。
##### 6.1.1 简单处理本地项目
&nbsp; &nbsp; 选中DoNotCallHellWorld右键Import Projects，选择 - Import as general project
>*[图6-1 Eclipse 导入仓库中的项目图01]*<br/>
![图6-1 Eclipse 导入仓库中的项目图01](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-16.png)<br/>
*[图6-2 Eclipse 导入仓库中的项目图02]*<br/>
![图6-2 Eclipse 导入仓库中的项目图02](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-17.png)

&nbsp; &nbsp; 点击Finish后会在开发工具生成本地项目
>*[图6-3 Eclipse 导入仓库中的项目成功图]*<br/>
![图6-3 Eclipse 导入仓库中的项目成功图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-18.png)

&nbsp; &nbsp; 当前项目还不是一个Java Project。选中项目右键选中功能选项Configure - Convert to ...选择项目类型；此处以基本的Java Project(Convert to Facets Project)为例。在弹出的对话框中选中Java选项选中1.7版本或者其他。Apply - OK即可。
>*[图6-4 Configure 配置为Java Project 图]*<br/>
![图6-4 Configure 配置为Java Project 图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-19.png)

&nbsp; &nbsp; 此时项目为Java Project。在当前项目下新建一个class。然后通过Git提交该项目到码云。
>*[图6-5 Java Project 图]*<br/>
![图6-5 Java Project 图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-20.png)

##### 6.1.2 提交项目到码云
&nbsp; &nbsp; 在Git Repositories视图下，右键项目选中Commit；填写Commit message，选择要提交的文件。然后点击Commit进行提交。
>*[图6-6 Commit 提交文件图]*<br/>
![图6-6 Commit 提交文件图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-21.png)

&nbsp; &nbsp; Commit命令还没有将文件上传到码云，Push才是真正将本地项目通过Git上传到码云的命令。还是在当前视图右键项目选中Push to Upstream，OK即可。
>*[图6-7 推送提交信息到码云图]*<br/>
![图6-7 推送提交信息到码云图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-22.png)

&nbsp; &nbsp; 回到码云查看该项目，发现项目文件已经提交完成。
>*[图6-8 码云查看用户提交记录图]*<br/>
![图6-8 码云查看用户提交记录图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-23.png)

&nbsp; &nbsp; 再回到本地开发工具，可以看到通过Git提交成功的文件会有特殊标识，而新建的TestPush.java没有，代表这是新增的文件还没有通过Git提交没有同步。Commit - Push更新到码云即可。
>*[图6-9 本机项目未提交文件状态图]*<br/>
![图6-9 本机项目未提交文件状态图](https://raw.githubusercontent.com/niaonao/ImageIcon/master/Git/Eclipse/1-24.png)

